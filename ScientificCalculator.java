
public class ScientificCalculator {
    
    private static Math math;

    public double tan(double a){
        return Math.atan(a);
    }

    public double sin(double a){
        return Math.sin(a);
    }

    public double cos(double a){
        return Math.cos(a);
    }

    public double cot(double a){
        return Math.atan(a);
    }
    public double cosec(double a){
        return Math.acos(a);
    }
}