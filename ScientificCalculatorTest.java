public class ScientificCalculatorTest {
    public static void main(String args[]) {
        ScientificCalculator calculator = new ScientificCalculator();
        double result = calculator.cos(34);
        double sinResult = calculator.sin(45);
        System.out.printf("The result is %d ", result);
        System.out.println("The sin of 45 is "+ sinResult);
    }
}