public class CalculatorClient {
    public static void main(String args[]){
        Calculator calculator = new Calculator();
        int result = calculator.multiply(34, 45);
        System.out.println("The product is "+ result);

        int divisionResult= calculator.divide(45, 5);
        System.out.println("Result of division is "+ divisionResult);
    }
}